const yup = require('yup');
 
const errors = {
    email: {
        stringMsg: "Email must be a string type",
        validity: "Email is invalid",
        required: "Email is required"
    },
    password: {
        stringMsg: "Password must be a string type",
        minMsg: "Password must contain minimum 8 characters",
        required: "Password is required"
    },
    confirmPassword: {
        stringMsg: "Password must be a string type",
        refMsg: "Passwords must match",
        required: "You must confirm the password"
    }
}
 
const schema = yup.object().shape({
    email: yup.string(errors.email.stringMsg).email(errors.email.validity).required(errors.email.required),
    password: yup.string(errors.password.stringMsg).min(8, errors.password.minMsg).required(errors.password.required),
    confirmPassword: yup.string(errors.confirmPassword.stringMsg).oneOf([yup.ref('password')], errors.confirmPassword.refMsg).required(errors.confirmPassword.required)
}).strict(true);
const data = {
    email: "vinishah@gmail.com",
    password: 'Welcome12#',
    confirmPassword: 'Welcome12#'
};
 
errorObj = {}
schema.validate(data, {abortEarly:false})
    .then(validData => console.log('Data is valid:', validData))
    .catch(errors => {
        errors.inner.forEach(err=>{
            errorObj[err.path] = err.message;
        })
        console.log(errorObj);
    });
 