const express = require('express');
const yup = require('yup');
 
const app = express();
app.use(express.json());
const port = 3000;
const regex = /^[0-9]{10}$/;
 
let userSchema = yup.object().shape({
 
    name: yup.string().required().label('User Name'),
 
    email: yup.string().email().required(),
 
    password: yup.string().min(5).max(15).required(),
 
    mobileNumber: yup.string().matches(regex, 'Invalid format').required(),
 
    image: yup.string().url().required(),
 
    jobs: yup.array().of(yup.string()).required(),
 
    isAdmin: yup.boolean().required()
}).strict(true)
 
const validateRequest = async (req, res, next) => {
    try {
        await userSchema.validate(req.body);
        next();
    } catch (error) {
        res.status(400).json({ error: error.message });
    }
};
 
// Route handler for POST request
app.post('/register', validateRequest, (req, res) => {
 
    // Process valid request
    const response = {
        data: req.body,
        message: 'Data submitted successfully'
    }
    res.status(200).json(response);
});
 
app.listen(port, () => {
    console.log(`Server is running on port:${port}`);
});
 