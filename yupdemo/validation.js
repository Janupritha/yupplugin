 
const yup = require('yup');
 
const express = require('express')
 
const app = express()
 
const regex = /^[0-9]{10}$/;
 
const userSchema = yup.object().shape({
 
    name: yup.string().required().label("Name"),   // typeError + lable
 
    age : yup.number().integer().positive().required().label("Age"),
  
    email: yup.string().email().required(),
 
    password: yup.string().min(5).max(15).required(),
 
    confirmPassword: yup.string().when('password', {
        is: (password) => password && password.length > 0,
        then: () => yup.string().oneOf([yup.ref('password')]).required(),
        otherwise: () => yup.string().required('password is required')
    }),
    
    mobileNumber:yup.string().matches(regex, 'Invalid format').required(),
 
    image:yup.string().url().required(),
 
    jobs:yup.array().of(yup.string()).required(),
 
    isAdmin:yup.boolean().required(),
 
    isAdult: yup.boolean().test('isAdult', 'Invalid age for adult', function(value) {          // test
        const { age } = this.parent;
        return age > 18 ? value === true : false;
    })
 
}).strict(true);
 
 
const data = {
   
    name:"Ram",

    age:20,
 
    email:"ram@gmail.com",
 
    password:"password123",
 
    confirmPassword:"password123",
 
    mobileNumber:"1234567890",
 
    image:"https://example.com",
 
    jobs:["engineer","doctor"],
 
    isAdmin:true,
 
    isAdult:true
}
 
 
userSchema.validate(data,{abortEarly:false}).then(data => console.log(data)).catch(err => console.log(err.errors))